import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lab01';
  name = 'SE331';
  students = [{
	  id: 1,
	  studentId: '60211507',
	  name: 'Prayuth',
	  surname: 'Tu',
	  gpa: 4.00
  }, {
	  id: 2,
	  studentId: '60211509',
	  name: 'Pu',
	  surname: 'Priya',
	  gpa: 2.12
  }, {
	  id: 3,
	  studentId: '602115023',
	  name: 'Somruk',
	  surname: 'Laothamjinda',
	  gpa: 3.98
  }
  ];
  averageGpa() {
	  let sum = 0;
	  for (let student of this.students) {
		  sum+=student.gpa;
	  }
	  return sum/this.students.length;
  }
  time = new Date();
}
